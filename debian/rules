#!/usr/bin/make -f

include /usr/share/dpkg/default.mk

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_LDFLAGS_MAINT_APPEND = -Wl,-z,defs

PKGDIR=$(CURDIR)/debian/playerctl/

ignored_libraries := -Xlibplayerctl.a

meson_options =

ifeq ($(DEB_HOST_ARCH),$(DEB_BUILD_ARCH))
native_or_cross = native
else
native_or_cross = cross
meson_options += --cross-file=$(DEB_HOST_GNU_TYPE)-gobject-introspection.ini
endif

%:
	dh $@ --with gir --buildsystem=meson

override_dh_auto_configure:
	dh_auto_configure -- $(meson_options) $(NULL)

override_dh_auto_build:
	dh_auto_build
	# build custom manpage for playerctld
	@echo DEB_VERSION_UPSTREAM $(DEB_VERSION_UPSTREAM)
	sed -e "s/__VERSION__/$(DEB_VERSION_UPSTREAM)/g" \
				< debian/playerctld.1.md.in > debian/playerctld.1.md
	pandoc debian/playerctld.1.md -s -f markdown -t man -o debian/playerctld.1

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	PATH="$(CURDIR)/obj-$(DEB_HOST_GNU_TYPE)/playerctl:${PATH}" \
			dbus-run-session pytest test \
			--deselect test/test_basics.py::test_system_list_players \
			--deselect test/test_daemon.py::test_daemon_commands \
			--deselect test/test_daemon.py::test_daemon_follow \
			--deselect test/test_daemon.py::test_daemon_shift_simple \
			--deselect test/test_daemon.py::test_daemon_shift_no_player \
			--deselect test/test_daemon.py::test_active_player_change
endif

# All binary packages get the upstream changelog
override_dh_installchangelogs:
	dh_installchangelogs -A CHANGELOG.md

override_dh_install:
	dh_install ${ignored_libraries}
	# Install zsh completions
	mkdir -p $(PKGDIR)/usr/share/zsh/vendor-completions/
	install -m0644 -p $(CURDIR)/data/playerctl.zsh \
		$(PKGDIR)/usr/share/zsh/vendor-completions/_playerctl
	# install bash completions
	mkdir -p $(PKGDIR)/usr/share/bash-completion/completions/
	install -m0644 -p $(CURDIR)/data/playerctl.bash \
		$(PKGDIR)/usr/share/bash-completion/completions/playerctl

override_dh_missing:
	dh_missing ${ignored_libraries} --fail-missing
